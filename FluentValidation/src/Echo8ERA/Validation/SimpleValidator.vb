Imports System
Imports System.Runtime.CompilerServices

Namespace Echo8ERA.Validation

	Public Module SimpleValidator

		<Extension>
		Public Function IsValid(Of TArg)(ByVal arg As TArg, ByVal condition As Boolean, ParamArray ByVal exceptionArgs As Object()) As TArg

			Return arg.IsValid(Of ArgumentException)(condition, exceptionArgs)

		End Function

		<Extension>
		Public Function IsValid(Of TArg, TException As Exception)(ByVal arg As TArg, ByVal condition As Boolean, ParamArray ByVal exceptionArgs As Object()) As TArg

			If condition Then Return arg

			Throw DirectCast(Activator.CreateInstance(GetType(TException), exceptionArgs), TException)

		End Function

		<Extension>
		Public Function IsValid(Of TArg)(ByVal arg As TArg, ByVal condition As Boolean, ByVal exception As Exception) As TArg

			If condition Then Return arg

			Throw exception

		End Function

		<Extension>
		Public Function IsValid(Of TArg, TException As Exception)(ByVal arg As TArg, ByVal condition As Boolean, ByVal exceptionFunc As Func(Of TException)) As TArg

			If condition Then Return arg

			Throw exceptionFunc()

		End Function

		<Extension>
		Public Function IsValid(Of TArg, TException As Exception)(ByVal arg As TArg, ByVal condition As Boolean, ByVal exceptionFunc As Func(Of TArg, TException)) As TArg

			If condition Then Return arg

			Throw exceptionFunc(arg)

		End Function

	End Module

End Namespace