Imports System

Imports Echo8ERA.Validation
Imports NUnit.Framework

Namespace Tests.Echo8ERA.Validation

	Partial Class SimpleValidator

		Partial Public Class IsValid

			Public Class TArg__TArg_Boolean_ObjectArray

				<Test>
				Public Sub TrueCondition_ReturnsOriginal()

					Dim o As New DummyClass()

					Dim r = o.IsValid(True, Constants.NoExceptionExpected, NameOf(o))

					Assert.That(r, [Is].SameAs(o))

				End Sub

				<Test>
				Public Sub FalseCondition_ThrowsArgumentException()

					Dim o As New DummyClass()

					Dim ex = Assert.Catch(Sub() o.IsValid(False, Constants.ExceptionExpected, NameOf(o)))

					Assert.Multiple(Sub()
							Assert.That(ex, [Is].TypeOf(Of ArgumentException)())

							Dim argEx = DirectCast(ex, ArgumentException)

							Assert.That(argEx.ParamName, [Is].EqualTo(NameOf(o)))
							Assert.That(argEx.Message, Does.StartWith(Constants.ExceptionExpected))
						End Sub)

				End Sub

			End Class

		End Class

	End Class

End Namespace