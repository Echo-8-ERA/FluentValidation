Imports System

Imports Echo8ERA.Validation
Imports NUnit.Framework

Namespace Tests.Echo8ERA.Validation

	Partial Class SimpleValidator

		Partial Public Class IsValid

			Public Class TArg_TException__TArg_Boolean_FuncOfTArgTException

				<Test>
				Public Sub TrueCondition_ReturnsOriginal()

					Dim o As New DummyClass()

					Dim r = o.IsValid(True, Function(arg) New ArgumentOutOfRangeException(NameOf(o), arg, Constants.NoExceptionExpected))

					Assert.That(r, [Is].SameAs(o))

				End Sub

				<Test>
				Public Sub FalseCondition_ThrowsArgumentOutOfRangeException()

					Dim o As New DummyClass()

					Dim ex = Assert.Catch(Sub() o.IsValid(False, Function(arg) New ArgumentOutOfRangeException(NameOf(o), arg, Constants.ExceptionExpected)))

					Assert.Multiple(Sub()
							Assert.That(ex, [Is].TypeOf(Of ArgumentOutOfRangeException)())

							Dim argEx = DirectCast(ex, ArgumentOutOfRangeException)

							Assert.That(argEx.ParamName, [Is].EqualTo(NameOf(o)))
							Assert.That(argEx.ActualValue, [Is].SameAs(o))
							Assert.That(argEx.Message, Does.StartWith(Constants.ExceptionExpected))
						End Sub)

				End Sub

			End Class

		End Class

	End Class

End Namespace