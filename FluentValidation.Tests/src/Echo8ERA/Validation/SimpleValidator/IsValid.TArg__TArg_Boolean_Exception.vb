Imports System

Imports Echo8ERA.Validation
Imports NUnit.Framework

Namespace Tests.Echo8ERA.Validation

	Partial Class SimpleValidator

		Partial Public Class IsValid

			Public Class TArg__TArg_Boolean_Exception

				<Test>
				Public Sub TrueCondition_ReturnsOriginal()

					Dim o As New DummyClass()
					Dim oex As New ArgumentOutOfRangeException(NameOf(o), o, Constants.ExceptionExpected)

					Dim r = o.IsValid(True, oex)

					Assert.That(r, [Is].SameAs(o))

				End Sub

				<Test>
				Public Sub FalseCondition_ThrowsArgumentException()

					Dim o As New DummyClass()
					Dim oex As New ArgumentException(Constants.ExceptionExpected, NameOf(o))

					Dim ex = Assert.Catch(Sub() o.IsValid(False, oex))

					Assert.That(ex, [Is].SameAs(oex))

				End Sub

			End Class

		End Class
	End Class
End Namespace